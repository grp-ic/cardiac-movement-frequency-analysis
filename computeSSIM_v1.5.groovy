/*
 * A Fiji groovy script to analyze periodic image structural changes
 * 
 * It use a user defined reference frame, 
 * to compute frame pair-wise strucutral similarity index SSIM:
 * as defined in "Wang, et al., 2004, doi: 10.1109/TIP.2003.819861."
 * https://ieeexplore.ieee.org/document/1284395
 * 
 * Then it use 1D fourier transform (FHT) class to do frequency analysis.
 * 
 * To use it, open the time series image stack (tested only for 8-bit),
 * and make sure it is the active image window, and click "Run" in script editor.
 * Provide a reference frame, which better to be the 1st frame.
 * Also indicate the frame rate (fps), the default is 30 fps (or 30Hz).
 * 
 * Author: Ziqiang Huang <ziqiang.huang@embl.de>
 * date: 2021.08.13
 * version: v1.5	added irregular shape ROI, and multiple ROI, 
 */

//#@ ImagePlus (label = "input dataset") imp
//#@ ImagePlus (label = "...ignore duplicate") imp2
#@ Integer	(label = "reference frame (0 as average)", value = 0, min = 0, max = 600, persist = true, style = "slider") t_ref
#@ Boolean	(label = "display SSIM plot", persist = true) showSSIM
#@ Boolean	(label = "display frequency spectrum", persist = true) showFreq
#@ Boolean	(label = "new plot for each ROI", persist = true) splitPlot

import java.awt.Color;
import java.awt.Point;
import ij.*;
import ij.gui.*;
import ij.plugin.*;
import ij.plugin.frame.RoiManager;
import ij.process.*;
import ij.measure.ResultsTable;

Color[] plotColors = [Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.CYAN, Color.MAGENTA, Color.BLACK];

	// timing the start
	double start = System.currentTimeMillis();
	// frame rate by default is 30 frame per second
	double fps = 30d;
	
	// get active image
	ImagePlus imp = IJ.getImage();
	
	// control input image dimension
	if (!imp.isStack() || imp.isHyperStack()) {
		IJ.log(imp.getTitle() + " : image dimension wrong!\n"
		+	" C:" + imp.getNChannels() + " Z:" + imp.getNSlices() + " T: " + imp.getNFrames());
		return;
	}
	Roi roi = imp.getRoi();					// store active selection if there's any
	boolean doRoi = (roi != null);			// check if ROI exist;

	Roi[] rois;
	if (roi!=null && roi.getType() == Roi.COMPOSITE)	{	// check if it's multiple ROI
		rois = ((ShapeRoi)roi).getRois();	// store multiple ROI
	} else {								// store single ROI as 1-element array
	 	rois = [ roi ];						// could be [ null ] for no ROI case
	}
	int nRoi = rois.length;
	if (doRoi && rois[0]!=null) {	// prepare ROI-Manager to store ROIs
		RoiManager rm = RoiManager.getInstance();
		if (rm==null) rm = new RoiManager();	
		else rm.reset();
		for (int i=0; i<nRoi; i++) {
			rm.addRoi(rois[i]);
			rm.rename(rm.getCount()-1, ""+(i+1));
		}
	}
	
	ArrayList<Point[]> roiPoints = new ArrayList<Point[]>();	// store ROI pixel coordinates as List of Point[]
	for (Roi r : rois) { 
		roiPoints.add( r==null ? null : r.getContainedPoints()); 
	}
	
	int nFrames = imp.getStack().getSize();	// ignore dimension and convert all slices to time frames
	t_ref = (int) Math.min(t_ref, nFrames);	// control refrence frame max bound
	imp = HyperStackConverter.toHyperStack(imp, 1, 1, nFrames);
	imp.getCalibration().setTimeUnit("sec");
	imp.getCalibration().frameInterval = 1/fps;
	// prepare result table
	ResultsTable table1 = new ResultsTable();
	table1.setPrecision(10);
	
	// get reference frame, whole image
	ImageProcessor ip1 = new ByteProcessor(imp.getWidth(), imp.getHeight());
	if (t_ref == 0) {
		ip1 = ZProjector.run(imp,"avg").getProcessor();
	} else {
		ip1 = imp.getStack().getProcessor(t_ref);
	}
	
	ArrayList<int[]> data1 = new ArrayList<int[]>();		// store 1D pixel value as int array for each ROI, as list
	//ArrayList<double[]> stat1 = new ArrayList<double[]>();	// store [mean, stdDev] for each ROI, as list
	double[] mean_1 = new double[nRoi];
	double[] std_1 = new double[nRoi];
	
	for (Point[] points : roiPoints) {
		data1.add( getIntArray1D(ip1, points) );
	}
	for (int i=0; i<nRoi; i++) {
		ip1.setRoi(rois[i] as Roi);
		//stat1.add( [ip.getStats().mean, ip.getStats().stdDev] );
		mean_1[i] = ip1.getStats().mean;
		std_1[i] = ip1.getStats().stdDev;
	}

	
	//int[][] data1 = ip1.getIntArray();
	//double[] stat1 = getMeanAndStdFast(data1, true);
	//double mean_1 = stat1[0]; double std_1 = stat1[1];

	// store values for result table and plot
	double[] timeLapse = new double[nFrames];
	//double[][] corr_coeff = new double[nRoi][nFrames];
	//double[][] pearson_corr_coeff = new double[nRoi][nFrames];
	double[][] SSIM_value = new double[nRoi][nFrames];
	// data for frequency spectra analysis with Discrete Hartley Transform (FHT in ImageJ)
	double[] data_sum = new double[nRoi];
	float[][] dataForFHT = new float[nRoi][nFrames];

	// loop over frames in data, for each frame do the defined processing
	for (int t=0; t<nFrames; t++) {
		IJ.showStatus("   Computing structural similarity for frame: " + (t+1));
		IJ.showProgress(t, nFrames);
		// get time lapse in second
		timeLapse[t] = t/fps;
		// add current time to a new row in result table
		table1.incrementCounter();
		table1.addValue("frame", ""+(t+1));
		table1.addValue("time lapse", ""+IJ.d2s(timeLapse[t],2)+" sec");

		// loop over ROIs
		for (int r=0; r<nRoi; r++) {
			ImageProcessor ip2 = imp.getStack().getProcessor(t+1);
			int[] data2 = getIntArray1D (ip2, roiPoints.get(r));
			ip2.setRoi(rois[r] as Roi);
			//stat1.add( [ip1.getStats().mean, ip1.getStats().stdDev] );
			double mean_2 = ip2.getStats().mean;
			double std_2 = ip2.getStats().stdDev;
			double corr_coeff = getCorrCoeff(data1.get(r), data2, mean_1[r], mean_2);
			//double pearson_corr_coeff = getPearsonCorrCoeff(data1.get(r), data2, mean_1[r], mean_2, std_1[r], std_2);
			SSIM_value[r][t] = getSSIM(mean_1[r], mean_2, std_1[r], std_2, corr_coeff);
			// for plot
			data_sum[r] += SSIM_value[r][t];
			dataForFHT[r][t] = (float) SSIM_value[r][t];
			
			//table1.addValue("correlation coefficient ROI " + (r+1), corr_coeff);
			//table1.addValue("pearson coefficient ROI " + (r+1), pearson_corr_coeff);
			table1.addValue("SSIM ROI " + (r+1), SSIM_value[r][t]);
		}
	}
	table1.show("SSIM measurement - " + imp.getTitle());

	
	if (showSSIM) {	// plot SSIM result
		String plotName = "SSIM plot" + (splitPlot ? " ROI 1" : "") +" - " + imp.getTitle();
		Plot plot1 = new Plot(plotName, "Time (second)", "SSIM value");
		plot1.setLineWidth(2); plot1.setColor(plotColors[0]);
		plot1.add("line", timeLapse, SSIM_value[0]);
		for (int r=1; r<nRoi; r++) {
			Plot plot1sub = splitPlot ? new Plot("SSIM plot ROI " + (r+1) + " - " + imp.getTitle(), "Time (second)", "SSIM value") : plot1;
			plot1sub.setLineWidth(2); plot1sub.setColor(splitPlot ? plotColors[0] : plotColors[r]);
			plot1sub.add("line", timeLapse, SSIM_value[r]);
			if (splitPlot) plot1sub.show();
		}
		plot1.show();
	}
	if (showFreq) {	// plot frequency result
		String plotName = "Frequency spectrum" + (splitPlot ? " ROI 1" : "") +" - " + imp.getTitle();
		plot2 = new Plot(plotName, "Frequency (Hz)", "RMS Amplitude");
		// prepare data for FHT
		float data_mean = (float) (data_sum[0] / nFrames);
		for (int t=0; t<nFrames; t++) {
			dataForFHT[0][t] -= data_mean;	// normalize data
		}
		// frequency analysis: 1D fourier transform
		float[] RMSAmp = new FHT().fourier1D(dataForFHT[0], FHT.HANN);
		int length = RMSAmp.length;		
		double[] freq = new double[length];
		double[] RMSAmpD = new double[length];
		for (int i=0; i<length; i++) {
			freq[i] = i * fps / (2* length);
			RMSAmpD[i] = (double) RMSAmp[i];
		}
		plot2.setLineWidth(1); plot2.setColor(splitPlot ? plotColors[6] : plotColors[0]);
		plot2.add("line", freq, RMSAmpD);

		
		for (int r=1; r<nRoi; r++) {
			Plot plot2sub = splitPlot ? new Plot("Frequency spectrum ROI " + (r+1) + " - " + imp.getTitle(), "Time (second)", "SSIM value") : plot2;
			data_mean = (float) (data_sum[r] / nFrames);
			for (int t=0; t<nFrames; t++) {
				dataForFHT[r][t] -= data_mean;	// normalize data
			}
			RMSAmp = new FHT().fourier1D(dataForFHT[r], FHT.HANN);
			length = RMSAmp.length;		
			freq = new double[length];
			RMSAmpD = new double[length];
			for (int i=0; i<length; i++) {
				freq[i] = i * fps / (2* length);
				RMSAmpD[i] = (double) RMSAmp[i];
			}
			plot2sub.setLineWidth(1); plot2sub.setColor(splitPlot ? plotColors[6] : plotColors[r]);
			plot2sub.add("line", freq, RMSAmpD);
			if (splitPlot) plot2sub.show();
		}
		plot2.show();
	}
	
	// release RAM
	IJ.run("Collect Garbage", "");
	System.gc();

	// timing the end
	double end = System.currentTimeMillis();
	println((end-start)/1000 + " second");


	// get pixel value of image in 1D int array
	public int[] getIntArray1D (ImageProcessor ip, Point[] points) {
		if (ip == null) return null;
		if (points == null) {	// intput ROI is null, return whole image as 1D int array
			int[][] data_2D = ip.getIntArray();
			int nRow = data_2D.length;
			int nCol = data_2D[0].length;
			int N = nRow * nCol;
			int[] data_1D = new int[N];
			for (int i=0; i<nRow; i++) {
				for (int j=0; j<nCol; j++) {
					data_1D[j + i*nCol] = data_2D[i][j];
				}
			}
			return data_1D;
		}
		int nPoints = points.length;
		int[] data = new int[nPoints];
		for (int i=0; i<nPoints; i++) {
			data[i] = ip.get((int)points[i].x, (int)points[i].y);
		}
		return data;
	}

	public double getSSIM (double mean_1, double mean_2, double std_1, double std_2, double corr_coeff) {
		double numerator = 4 * (mean_1 * mean_2 * corr_coeff) ;
		double denominator = (mean_1 * mean_1 + mean_2 * mean_2) * (std_1 * std_1 + std_2 * std_2) ;
		return (numerator / denominator);
	}


	public double getCorrCoeff (int[] x, int[] y, double x_mean, double y_mean) {
		// control inputs
		if (x==null || y==null) return null;
		int N = x.length;
		if (y.length != N) return null;
		if (N == 1) return null;
		double sum = 0.0;
		for (int i=0; i<N; i++) { sum += (x[i] - x_mean) * (y[i] - y_mean); }
		return ( sum / (N-1) );	
	}
	
	public double getPearsonCorrCoeff (int[] x, int[] y, double x_mean, double y_mean, double x_std, double y_std) {
		// control inputs
		if (x==null || y==null) return null;
		int N = x.length;
		if (y.length != N) return null;
		if (N == 1) return null;
		double sum = 0.0;
		for (int i=0; i<N; i++) {
			sum += (x[i] - x_mean) * (y[i] - y_mean);
		}
		sum /= (N-1) * x_std * y_std;
		return sum;	
	}
    
	public double getRMS (int[] data) {
		if (data == null) return null;
		int N = data.length;
		if (N == 0) return null;
		double sum2 = 0.0;
		for (int i=0; i<N; i++) {
			sum2 += data[i] * data[i];
		}
		return Math.sqrt(sum2/N);
	}
