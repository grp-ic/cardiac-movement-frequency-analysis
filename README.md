# cardiac-movement-frequency-analysis

A Fiji Groovy script to perform cardiac muscle movement analysis.<br/>
&nbsp;&nbsp;&nbsp;- (created to support user <fabrice.darche@embl.de>)


##  1.  Description:

To measure and analyze the movement (beating) frequency of the heart muscle from movie, we used a structural similarity approach implemented as a custom Fiji Groovy script. The structural similarity measurement was inspired by [1], and slightly modified to better fits the brightness and contrast, and SNR of our data. More specifically, we took a given time frame, or the time-average of the whole movie as our reference frame. Then we compute the pair-wise similarity (SSIM) value, between the reference frame and each time frame of the whole movie. The pair-wise similarity score can be computed either using whole field of view of the movie, or selected area as region-of-interest (ROI). This enables us to analyze overall changes in structural of the tissue over time, as well as local variations.

We then plot the SSIM value against time (Fig.1), to inspect temporal changes in structure of the tissue. For frequency analysis, we performed a 1D Fast Hartley Transform (FHT) [2] on the SSIM data (to Fabrice: if you also would like to include some frequency plot?).



##  2.  Usage:

Drag and drop Groovy script [computeSSIM_v1.5.groovy](/computeSSIM_v1.5.groovy) onto Fiji window to open it in Fiji's script editor. With an active time-lapse dataset (e.g.:[ sample data: control](/sample_data_ctr.gif)), click "Run" on script editor to run the Groovy script. It will then analyze the active dataset as described above.



##  3.  Sample Data:
- control data (fast movement)

    ![](/sample_data_ctr.gif?raw=true)
    ![](/ctr_result.png?raw=true)

    
- mutant data (slow movement)

    ![](/sample_data_mutant.gif?raw=true)
    ![](/mutant_result.png?raw=true)



##  4.  References:

[ [1]  Zhou Wang, A. C. Bovik, H. R. Sheikh and E. P. Simoncelli, (April 2004). "Image quality assessment: from error visibility to structural similarity," in IEEE Transactions on Image Processing, vol. 13, no. 4, pp. 600-612. doi: 10.1109/TIP.2003.819861.](https://ieeexplore.ieee.org/document/1284395)

[ [2]  Hartley, Ralph V. L. (March 1942). "A More Symmetrical Fourier Analysis Applied to Transmission Problems". Proceedings of the IRE. 30 (3): 144–150. doi:10.1109/JRPROC.1942.234333](https://ieeexplore.ieee.org/document/1694454)
